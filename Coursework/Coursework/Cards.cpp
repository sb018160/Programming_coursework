﻿#include <iostream>
#include <vector>
#include <string>


#include "Cards.h"
using namespace std;


void PrintTerrain(char ID)
{
	vector<string> top = { " ____________________","/ __________________ \\","| |                | |" };
	vector<string> bottom = { "| |________________| |","\\____________________/ " };
	vector<string> Forest = { "| |@@@@@@@@@@@@@@@@| |","| |@@@@@@@@@@@@%@@@| |","| |@@@@@@@%@@@@%@@@| |","| |%@@@@@%%@@%%%@@@| |","| |%%@@@%%%@%%%@%%%| |","| |%%%%@%%%%%%%%%%%| |","| |%%%%%%%%% %%%%%%| |","| | %{}{%%% .   {} | |","| |  }{}    .   {} | |","| |  {}{   ..   }} | |","| |  }}{   ..  /}{\\| |","| |  {}}  ...      | |","| | /{{}\\ ...      | |","| |       ...      | |" };
	vector<string> Fields = { "| |  ooo     oooo  | |","| | ooooo   ooooooo| |","| | oo           oo| |","| |                | |","| |                | |","| |_/_,_\\__,_,_\\/__| |","| |, '   .  ,   '  | |","| | .  '  ,  ,  '  | |","| | ,   .        , | |","| |  ,,  ,.  '  ', | |","| |,'  .',   ',.'  | |","| |',. '.,',. ',,.,| |","| | ,.'.,..' ,.','.| |","| | ,. ,.'.,'.,'',.| |" };
	vector<string> River = { "| |                | |","| |                | |","| |                | |","| |                | |","| |                | |","| |_/_,_     _,_\\/_| |","| |, '  |^ ^|   '  | |","| | .  | ^   |  '  | |","| | , |.    ^ |  , | |","| |  |  ^   ^  |', | |","| |,/ _ ^   |  ^\\  | |","| |/   ..    ,  _ \\| |","| | __  _  _   ^   | |","| | ^   ^  ,  ^  _ | |" };

		for (int j = 0; j < 3; j++)
		{

			cout << top[j] << endl;

		}
		for (int j = 0; j < Forest.size(); j++)
		{
				switch (ID)
				{
				case '!':
				{
					cout << Forest[j];
					break;
				}
				case '-':
				{
					cout << Fields[j];
					break;
				}
				case '~':
				{
					cout << River[j];
					break;
				}
				default :
				{
					cout << "If failed" << endl;
					break;
				}
				}
				cout << endl;
		}
		for(int j = 0; j < 2; j++)
		{
			cout << bottom[j] << "		";
			cout << endl;
		}
}


void PrintMonsters(vector<char> ID)
{
	vector<string> Oog = { " _______________","/ _____________ \\","| |           | |","| |   _____   | |","| |  /  _  \\  | |","| | |  |O|  | | |","| |  \\  ‾  /  | |","| |  /‾‾‾‾‾\\  | |","| |  \\     /  | |","| |   \\    \\  | |","| |    Oog    | |","| |___________| |","\\_______________/" };
	vector<string> Moop = { " _______________","/ _____________ \\","| |           | |","| |           | |","| |     _     | |","| |    |=|    | |","| | +   ‾\\    | |","| |  \\___||   | |","| |   \\  ||   | |","| |     /  \\  | |","| |    Moop   | |","| |___________| |","\\_______________/" };

}
void ListCards(int ID)
{
	switch (ID)
	{
	case 0:
	{
		cout << "Attack:  Hits normal strength" << endl;
		break;
	}
	case 1:
	{
		cout << "Attack +: Hits for strength *2" << endl;
		break;
	}
	case 2:
	{
		cout << "Attack ++: Hits for strength *3" << endl;
		break;
	}
	case 3:
	{
		cout << "Defend: Blocks 40%" << endl;
		break;
	}
	case 4:
	{
		cout << "Defend +: Blocks 70%" << endl;
		break;
	}
	case 5:
	{
		cout << "Defend ++: Blocks 100%" << endl;
		break;
	}
	case 6:
	{
		cout << "Heal: Heals 25%" << endl;
		break;
	}
	case 7:
	{
		cout << "Heal +: Heals 50%" << endl;
		break;
	}
	case 8:
	{
		cout << "Heal ++: Heals 90%" << endl;
		break;
	}
	case 9: 
	{
		cout << "Fire: Deals fire damage based on magic" << endl;
		break;
	}
	case 10:
	{
		cout << "Fire +: Deals fire damage based on magic *2" << endl;
		break;
	}
	case 11:
	{
		cout << "Fire ++: Deals fire damage based on magic *3" << endl;
		break;
	}
	case 12:
	{
		cout << "Water: Deals water damage based on magic" << endl;
		break;
	}
	case 13:
	{
		cout << "Water +: Deals water damage based on magic *2" << endl;
		break; 
	}
	case 14:
	{
		cout << "Water ++: Deals water damage based on magic *3" << endl;
		break;
	}
	}
}