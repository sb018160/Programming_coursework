#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>

#include "Character.h"
using namespace std;

Character::Character(int type)
{
	stats.resize(11);
	if (type < 3)
	{
		AllAbilityID.resize(2);
		AllConsumeID.resize(2);
	}
	switch (type)
	{
	case 0: //Knight class
	{ 
		stats[0] = 1;
		stats[1] = 10;
		stats[2] = 0;
		stats[3] = 20;
		stats[4] = 20;
		stats[5] = 10;
		stats[6] = 6;
		stats[7] = 3;
		stats[8] = 1;
		stats[9] = 0;
		stats[10] = 0;

		break;
	}
	case 1: //Rogue class
	{
		stats[0] = 1;
		stats[1] = 10;
		stats[2] = 0;
		stats[3] = 15;
		stats[4] = 15;
		stats[5] = 10;
		stats[6] = 4;
		stats[7] = 8;
		stats[8] = 3;
		stats[9] = 0;
		stats[10] = 0;
		break;
	}
	case 2: //Mage class
	{
		stats[0] = 1;
		stats[1] = 10;
		stats[2] = 0;
		stats[3] = 15;
		stats[4] = 15;
		stats[5] = 10;
		stats[6] = 2;
		stats[7] = 5;
		stats[8] = 8;
		stats[9] = 0;
		stats[10] = 0;
		break;
	}
	case 3: //Default
	{
		stats[0] = 0;
		stats[1] = 0;
		stats[2] = 0;
		stats[3] = 0;
		stats[4] = 0;
		stats[5] = 0;
		stats[6] = 0;
		stats[7] = 0;
		stats[8] = 0;
		stats[9] = 0;
		stats[10] = 0;
		break;
	}
	}
	if (type < 3)
	{
		MaxCP = stats[5];
		UsedCP = 0;
	}
}

void Character::ReStatEnemy(Character enemy, int Type)
{

	enemy.stats[0] = stats[0];
	switch (Type)
	{
	case 0: //Oog
	{
		enemy.MonstType = '0';
		enemy.stats[3] = (10 + stats[0] * 1.5);
		enemy.stats[4] = enemy.stats[3];
		enemy.stats[6] = (3 + stats[0] * 0.8);
		enemy.stats[7] = (7 + stats[0] * 1.2);
		enemy.stats[8] = (4 + stats[0] * 1.2);
		enemy.stats[9] = -2;
		enemy.stats[10] = 5;
		break;
	}
	case 1: //Moop
	{
		enemy.MonstType = '0';
		enemy.stats[3] = (15 + stats[0] * 1.5);
		enemy.stats[4] = enemy.stats[3];
		enemy.stats[6] = (7 + stats[0] * 1.5);
		enemy.stats[7] = (3 + stats[0] * 0.5);
		enemy.stats[8] = (0 + stats[0] * 1.5);
		enemy.stats[9] = 2;
		enemy.stats[10] = -1;
		break;
	}
	}

}
void Character::AddCard(int ID, bool starting)
{
	if (starting)
	{
		if (ID < 50)
		{
			AllAbilityID[0].push_back(ID);
			AllAbilityID[1].push_back(0);
		}
		else
		{ 
			AllConsumeID[0].push_back(ID);
			AllConsumeID[1].push_back(0);
		}
	}
}
void Character::EquipCard(bool deck, bool starting, int position)
{
	if (starting)
	{
		if (deck)
		{
			if (AllConsumeID[1][position] == 0)
			{
				AllConsumeID[1][position] = 1;
			}
			else
			{

			}
		}
		else
		{
			if (AllAbilityID[1][position] == 0)
			{
				AllAbilityID[1][position] = 1;
			}
			else
			{

			}
		}
	}
}


int attack(Character enemy);

int gethit(Character enemy);

void getitem(int gold, vector<string> items);

void getxp(int amount);

void levelup();

void Unequip();

void equip();
