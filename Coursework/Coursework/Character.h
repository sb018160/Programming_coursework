#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Character
{
public: 
	Character(int type);
	void attack(Character enemy);
	void gethit(Character enemy);
	void getitem(int gold, vector<string> items);
	void getxp(int amount);
	void levelup();
	void CharMenu();
	void Unequip();
	void equip();
	void AddCard(int ID, bool starting);
	void EquipCard(bool deck, bool starting, int position);
	char MonstType;
	void ReStatEnemy(Character enemy, int type);
protected:
	vector<int> stats; //0 - level, 1 - needed xp, 2 - current xp, 3 - max hp, 4 - current hp, 5 - max cp, 6 - strength, 7 - agility, 8 - magic, 9 - DefenceP, 10 - DefenceM;   

	///////////////////// Inventory
	vector<vector<int>> AllEquipment; // 0 - head, 1 - body, 2- arms, 3 - legs, 4 - weapon.
	vector<bool> Equipflag;
	////////////////////// Cards
	vector<vector<int>> AllAbilityID; // 0 - id, 1 - flag.
	vector<vector<int>> AllConsumeID;
	int MaxCP;
	int UsedCP;
	
};