#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <ctime>

#include "Events.h"



using namespace std;


vector<Character> StartBattle(Character player, char location)
{
	vector<Character> Enemies;
	int battle = rand() % 6;
	switch (battle)
	{
	case 0:
	{ //Oog
		Enemies.push_back(4);
		player.ReStatEnemy(Enemies[0], 0);
		break;
	}
	case 1:
	{ //Oog,Oog
		Enemies.push_back(4);
		player.ReStatEnemy(Enemies[0], 0);
		Enemies.push_back(4);
		player.ReStatEnemy(Enemies[1], 0);
		break;
	}
	case 2:
	{ //Moop
		Enemies.push_back(4);
		player.ReStatEnemy(Enemies[0], 1);
		break;
	}
	case 3:
	{ //Moop,Moop
		Enemies.push_back(4);
		player.ReStatEnemy(Enemies[0], 1);
		Enemies.push_back(4);
		player.ReStatEnemy(Enemies[1], 1);
		break;
	}
	case 4:
	{ //Moop, Oog
		Enemies.push_back(4);
		player.ReStatEnemy(Enemies[0], 1);
		Enemies.push_back(4);
		player.ReStatEnemy(Enemies[1], 0);
		break;
	}
	case 5:
	{ //Oog, Moop
		Enemies.push_back(4);
		player.ReStatEnemy(Enemies[0], 0);
		Enemies.push_back(4);
		player.ReStatEnemy(Enemies[1], 1);
		break;
	}

	}
	return Enemies;
}
void StartEvent(Character player, char location);
void PrintMenu(Character player, vector<Character> Enemies)
{
	char input;
	system("CLS");
	vector<char> ID;
	for (int i = 0; i < Enemies.size(); i++)
	{
		ID.push_back(Enemies[i].MonstType);
	}
	cout << endl << endl << endl << "Use A bility,   Use C onsumable,   S truggle" << endl;
	cin >> input;
}
void UseCard(Character player);
void UseOnEnemy(Character player, Character enemy);
