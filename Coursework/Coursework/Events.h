#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <ctime>


#include "Cards.h"
#include "Character.h"
using namespace std;


vector<Character> StartBattle(Character player, char location);
void StartEvent(Character player, char location);
void PrintMenu(Character player,vector<Character> Enemies);
void UseCard(Character player);
void UseOnEnemy(Character player, Character enemy);
