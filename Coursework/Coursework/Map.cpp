#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <ctime>

#include "Map.h"
using namespace std;


/*
....................
.!!!!~!!T!------^^^.
.!!!~~!-!!---~~--^^.
.!!!~!--!------C-^^.
....................
*/
Map::Map(bool tutorialflag)
{
	SeedRand();
	if (tutorialflag)
	{
		MapCoords = { 108,315 };
		MapName = { "Cardville", "Carderia" };
		createmaptut();
	}
};


void Map::createmaptut()
{
	vector<string> mapS = { "....................",".!!!!~!!T!------^^^.",".!!!~~!-!!---~~--^^.",".!!!~!--!------C-^^.","...................." };
	map.resize(mapS.size());
	for (int i = 0; i < mapS.size(); i++)
	{
		map[i].assign(mapS[i].begin(), mapS[i].end());
	}
}

void Map::printmap(bool empty, int coords)
{
	for (int i = 0; i < map.size(); i++)
	{
		for (int I = 0; I < map[i].size(); I++)
		{
			if (empty)
			{
				cout << map[i][I];
			}
			else
			{
				if (coords == (i * 100 + I))
				{
					cout << "#";
				}
				else
				{
					cout << map[i][I];
				}
			}
		}
		cout << endl;
	}
}

void Map::travel(int PCoords, int maxdist, Character player)
{
	int Y = PCoords / 100;
	int X = PCoords % 100;
	vector<int> MapY;
	vector<int> TempY;
	vector<int> MapX;
	vector<int> TempX;
	vector<int> LocDistance;
	int counter = 1;
	int counter2;
	int input;
	for (int i = 0; i < MapCoords.size(); i++)
	{
		MapY.push_back(MapCoords[i] / 100);
		MapX.push_back(MapCoords[i] % 100); 
		if(MapY[i] >= MapX[i])
		{
			LocDistance.push_back(MapY[i] - Y);
			
		}
		else
		{
			LocDistance.push_back(MapX[i] - X);
		}
		if (LocDistance[i] < 0)
		{
			LocDistance[i] = LocDistance[i] - (2 * LocDistance[i]);
		}
	}
	for (int i = 0; i < LocDistance.size(); i++)
	{
		if (-(maxdist) <= LocDistance[i] && LocDistance[i] <= maxdist)
		{
			cout << counter << " " << map[MapY[i]][MapX[i]] << ":   " << MapName[i] << " is distance " << LocDistance[i] << " far away" << endl;
			NearbyCoords.push_back(MapCoords[i]);
			TempY.push_back(MapY[i] - Y);
			TempX.push_back(MapX[i] - X);
			counter++;
		}
		else
		{

		}
	}
	cout << "Select where to travel or type 0 to cancel" << endl;
	cin >> input;
	input--;
	if (input != -1)
	{
		if (input > NearbyCoords.size())
		{
			input = 0;
			cout << "Incorrect input, leaving travel menu" << endl;
		}
		else
		{
			cout << "Journey layout" << endl;
			for (int i = 0; i < LocDistance[input]; i++)
			{
				if (TempY[input] != 0)
				{
					counter = rand() % (TempY[input] - 1);
				}
				else
				{
					counter = 0;
				}
				if (TempX[input] != 0)
				{
					counter2 = rand() % (TempX[input] - 1);
				}
				else
				{
					counter2 = 0;
				}
				cout << map[Y + counter + 1][X + counter2 - 1] << " ";
				journeytiles.push_back(map[Y + counter + 1][X + counter2 - 1]);
			}
			cout << endl;
		}
		cout << "press 1 to accept or 0 to cancel" << endl;
		cin >> counter;
		if (counter)
		{
			Journey(LocDistance[input] -1, player);
		}
		else
		{
			cout << "Cancelling" << endl;
		}
	}
	
}



void Map::SeedRand()
{

	static bool randHasBeenSeeded = false;
	if (!randHasBeenSeeded)
	{
		srand(time(NULL));
		randHasBeenSeeded = true;

	}

}

void Map::Journey(int dist, Character player)
{
	int chance;
	for (int i = 0; i < dist; i++)
	{
		system("CLS");
		PrintTerrain(journeytiles[i]);
		chance = rand() % 2;
		if (/*chance == 0*/ 1) //Encounter, 50% chance
		{
			chance = rand() % 100; 
				if (/*chance < 60*/1) //Battle, 60% chance
				{
					cout << "Battle" << endl;
					PrintMenu(player, StartBattle(player, journeytiles[0]));
				}
				else //Random event
				{ 
					cout << "Random event" << endl;
				}
		}
		else //Nothing happens
		{
			cout << "Nothing happened" << endl;
		}
		cout << "Continue?" << endl;
			anykey();
	}
}

void anykey()
{
	system("PAUSE");
}

