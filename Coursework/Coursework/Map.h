#include <iostream>
#include <vector>
#include <string>

#include "Events.h"
using namespace std;

void anykey();

class Map
{
public:
	Map(bool tutorial);
	void SeedRand();
	void createmaptut();
	void printmap(bool empty, int coords);
	void travel(int coords, int maxdist, Character player);
	void Journey(int distance, Character player);
protected:
	vector<int> MapCoords;
	vector<string> MapName;
	vector<vector<char>> map;
	vector<int> NearbyCoords;
	vector<char> journeytiles;
};