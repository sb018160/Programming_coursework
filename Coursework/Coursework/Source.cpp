#include <iostream>
#include <vector>
#include <string>
#include <stdlib.h>

#include "Map.h"
using namespace std;
void tutorial();
void anykey();

int main()
{
	tutorial();
	return 0;
}


void tutorial()
{
	Character P(0);
	int PCoords = 108;
	Map M(1);
	cout << "this is an example for the concept, code used is different from the finished application and is used here for an example tutorial to teach you how to play" << endl;
	anykey();
	cout << "you will start of in a random town every time you play but for the sake of the tutorial, there are no random elements" << endl;
	anykey();
	system("CLS");
	M.printmap(1, PCoords);
	cout << "this is the map of the tutorial, the character on each tile represents the tile type" << endl << ". = border, ! = Woodland, - = plains, ~ = river/lake, ^ = mountain, T/C = Town/Castle"<< endl;
	anykey();
	system("CLS");
	cout << "Now lets go into the idle view" << endl;

	M.printmap(0, PCoords);

	cout << "E nter town.    T ravel.    C ards.   S tats." << endl << endl<< "# is your current location" << endl << "Press T to travel" << endl;

	char input = 'c';
	while (input != 'T')
	{
		cout << "Press T to continue (case sensitive)" << endl;
		cin >> input;
	}
	system("CLS");
	M.travel(PCoords, 10, P);
}
