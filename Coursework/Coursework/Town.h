#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Settlement
{
public:
	Settlement(int type);
	void enter();
	int Store(int money, int store);
	void hints();
protected:
	vector<int> Stores;
};