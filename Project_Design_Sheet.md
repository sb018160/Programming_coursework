# Project Design Sheet

### Introduction

I've chosen for my project to create a card based game, initially i had the idea to create an rpg style game with card based combat mechanics, after looking over other posibilities i've become set on this idea due to the creative freedom it gives me and as it allows me to create something new rather than a recreation of a simple card game.

### Analysis

For my game I've taken inspiration from many different games and thought about how to incorporate interesting mechanics from those games into mine. 
The main games I've taken inspiration from are:
* Kingdom Hearts - Chain of memories (https://en.wikipedia.org/wiki/Kingdom_Hearts:_Chain_of_Memories)
    * This game features a card based combat mechanic as well as deck building for those cards.
    * The player fights enemies using cards which each have a value assigned to them, the character with a higher value card wins the fight however the game also included much more complicated mechanics such as merging cards together to give higher values, this mechanic was the main inspiration for my game.
* Kingsway (http://store.steampowered.com/app/588950/Kingsway/)
    * A Rogue like RPG which inspired me to base my game on navigating around a large randomly generated world, completing quests and fighting monsters which would reward the player with experience which the player would eventually build enough of to level up and increase stats, due to the Rogue like nature of this game, every playthrough would be unique which is what i wanted my game to be like.
* Darkest Dungeon (http://store.steampowered.com/app/262060/Darkest_Dungeon/)
    * A Rogue like turn based strategy game in which the player has a group of mercenaries and explores randomly generated dungeons in which anything could happen with a high risk, high reward theme for the game. This also inspired me to create a randomly generated world in which every playthrough would be unique, however also inspired me to create a story which the player could progress through and so constantly have a set goal, as well as boss fights for the player to overcome.
* Dwarf fortess (http://www.bay12games.com/dwarves/)
    * A complicated town management game in which the player indirectly controls a group of dwarves by assigning them jobs and tasks to complete. The game is run in a console based layout and so all graphics are ascii based which is what i am going to be trying to accomplish.

### Requirements
#### Program Requirements
In the end, my program has these set requirements:
* Must be able to be a playable game.
* Must feature rogue like mechanics in which no two playthroughs are the same.
* Must feature a card based combat system.
* The card based combat system must be customizable by the player.
* Must feature an end goal.
* Must feature a navigation system that allows the player to explore an in game world.
* Must feature enemies for the player to fight.

#### Program Specification
* Rogue based gameplay
    * The game features a random element in which many elements of the game are random and so every playthrough of the game is unique, the random element could be in terms of what enemies to fight, a critical attack during combat, a dodge/block chance during combat, and the world design. All of these would cause every playthrough of the game to be different to the last, especially having a randomly generated world which would cause a completely different scenario for the player to explore every time they play the game.
* Card Based Combat System
    * The game features a deck building mechanic where the player can choose what cards they want in their card deck along with a limit on the maximum amount of cards the player can carry. These cards are to be used in battles that the player can come across in the game. 
* Navigation System
    * The player is able to navigate across a world map to different locations which can range from randomly created dungeons, towns (In which the player is able to purchase items). The map will also contain key locations in which the player can navigate to in order to progress through a quest however there will be barriers in order to stop the player from navigating anywhere on the map such as a maximum distance.
* Must Feature Enemies
    * The player will encounter enemies during navigation in the game, the encounter will be random and so is not guarenteed, the encountered enemies will also be random and will feature a small AI which can determine how it responds in situations such as healing when low on health or using a certain attack if required.


#### Comparison
The main two games that inspire mine are Kingdom hearts - Chain of memories and Kingsfield, whilst my game will take inspiration from mechanics in both of these games, it will not directly copy the mechanics as they will be changed in one way or another. For the card based combat mechanic, Kingdom hearts uses a number based system on it's combat where each card represents a certain ability such as attacking or using magic. Each card also has a number on it which represents the strength of the card. If the player and an enemy play a card at the same time, the higher value card cancels out the lower value card, stunning whoever played the lower value card and allowing the higher value's card ability to go off. Whilst this is an interesting mechanic I am going to use a similar system for each card representing an ability however i will not give cards a strength value and so any card the player may use will go off during battle.

In terms of Kingsfield, i will be using a similar type of navigation system in which the map has nodes where the player can chose to move to. The nodes can vary from being a specific location such as a dungeon or town, or just an empty node in which a random event could happen. The player has a limited range they can travel and so must travel from node to node to get to their end goal. I will also be using a node based system in which each node has a coordinate on the map and the player moves between these nodes, needing to use them as there will be a max travel distance the player can go, the distance between nodes would be used as "steps" during the journey and each step could cause a random event to happen, be it a battle or some kind of choice e.g. random chest encounter.

### Design

#### Decomposition

* Rogue based gameplay
    * The game will feature a random element which will be seeded every time the game is played, based on the current time when it is seeded. This means that as long as the game is not run at the exact same time, the outcome will be different always.
* Navigation system
    * The map will be a set size and ascii based with each character representing a certain terrain or location. The map will also feature "nodes" which will be locations the player can travel to, whether the player can travel to the location or not will be based on the distance between the player and the node which is calculated using whatever is longest, the horizontal or vertical distance between nodes. This distance will also be the amount of "steps" the player takes to get to the location
    * Each step uses the random element which determines if anything happens on the step, if something does happen then another roll is done to determine what happens, be it a battle or an encounter.
* Battle system
    * If during travel the player encounters a battle, then the battle scenario will also be randomly chosen which will determine how many enemies and what enemies will be in the battle. The enemies will scale with the players progress through the game which means that they will always serve a challenge to the player instead of the player progressing past the enemies and them becoming more of a waste of time.
    * During battle, the player will be dealt cards (Amount could be based on progression) from their deck, then in turn based combat the player will choose what card to pick and use, if the card can be used on an enemy then what enemy to use it on. 
    * Card abilities can vary from attacking, using magic (Enemies could have different resistances to physical and magical damage), using consumables (potions, bombs ect)
    * Once a card is used it cannot be regained during any other steps until a town is reached.
    * Enemy attacks will be mixed between a counter based system (e.g attack C is used every 3 turns) and a random based system. This will cause the enemy to have a basic AI in which it can be pushed to do certain actions such as heal based on it's missing health.

#### Flowchart
